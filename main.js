/*!
 * Copyright (C) 2012  Agnia Barsukova, Daniil Sorokin, Ildus Mukhametov, Vladlena Sergeeva, 
 * Department of General and Computational Linguistics, University of Tuebingen
 * 
 * This file is part of the ToponymVis project.
 *
 * ToponymVis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.*
 *
 * ToponymVis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details: <http://www.gnu.org/licenses/>.
 *
 * Contact: magpie.danial@gmail.com, agniabarsukova@gmail.com
 */



		/** Main map. */
		var myMap; 
		/** Array to store all the groups. All the groups in this array keep their initial position. This position is group's id.*/
		var groups = new Array(); 
		/** Array to store all toponyms.*/
		var toponymsArray = new Array();
		var toponymsIndexesArray = new Array();
		
		/**This array contain which groups and in which order should appear in the GUI.	*/
		var groupsIndexesArray = new Array(); 
		var currentSource; 
		/** Contains the ids of the selected groups. */
		var currentSelection = new Array();
		var currentSortType = "letters_back";
		var showPolygons = false;
		var useOverview = false;
		var populationDensityLayerVisability = false;
		
		var populationPolygonsArray = new Array();;
		
		var selectionPolyline;
		var selectionPolygon;
		var selectionMarker;
		
		/** This array contains the information about available data sets.*/
		var DataSources = [{name:"Ingria area", fileName: "toponyms_Ingria.txt", startPoint: new google.maps.LatLng(59.4, 29.13333), startZoom: 8,fileFormat: {name:0, latitude:2, longitude:3, formant:4, otherNames:1}}, 
						  //{name:"Leningrad region (old)", fileName: "toponyms_LenObl.txt", startPoint: new google.maps.LatLng(59.4, 31.13333), startZoom: 7, fileFormat: {name:0, latitude:1, longitude:2, formant:4, population:3, otherNames:5}},
						  //{name:"Baden-Württemberg (old)", fileName: "toponyms_BaWu.txt", startPoint: new google.maps.LatLng(48.5333, 9.05003), startZoom: 7, fileFormat: {name:0, latitude:1, longitude:2, formant:4, population:3}},
						  {name:"Leningrad region", fileName: "toponyms_LenObl_109.txt", startPoint: new google.maps.LatLng(59.4, 31.13333), startZoom: 7, fileFormat: {name:0, latitude:1, longitude:2, formant:4, population:3, otherNames:5}},
						  {name:"Baden-Württemberg", fileName: "toponyms_BaWu_97.txt", startPoint: new google.maps.LatLng(48.5333, 9.05003), startZoom: 7, fileFormat: {name:0, latitude:1, longitude:2, formant:4, population:3}}
						  ];
		
		var styleArray = [{featureType: "all", stylers: [{ saturation: -50 }]},
						{featureType: "road", elementType: "labels", stylers: [{ visibility: "off" }]},
						{featureType: "poi", elementType: "labels", stylers: [{ visibility: "off" }]},
						{featureType: "administrative.locality", elementType: "labels", stylers: [{ visibility: "off" }]}
						];

		var infowindow = new google.maps.InfoWindow();
							
		var DEFAULT_COLOR = "#707070";
		var DEFAULT_OPACITY = 0.8;
		var SELECT_COLOR = "#E8E8E8";
		var SELECTION_COLOR = "#FF0000";
		
		var notification;
		var screen;
		
		/**
		* Main function which should be called after the loading of the page.
		*/
		function initialize()
		{	
			var myOptions = {
					zoom:4, 
					center: new google.maps.LatLng(59.4, 29.13333),
					streetViewControl: false,
					scaleControl: true,
					overviewMapControl: true,					
					mapTypeId:google.maps.MapTypeId.ROADMAP
					};
			
			myMap = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
			myMap.setOptions({styles: styleArray});

			notification = document.getElementById("notificationWindow");
			screen = document.getElementById("screen");
			
			google.maps.event.addListener(myMap, 'click', function(){infowindow.close();});

			
			//var ctaLayer = new google.maps.KmlLayer('http://www.google.de/url?sa=t&rct=j&q=&source=web&cd=10&ved=0CEAQFjAJ&url=http%3A%2F%2Fwww.gelib.com%2Fwp-content%2Fuploads%2F2009%2Fnasa-earth-observation_nl.kml&ei=PrtdT5qyHYTOswaF4pmJDA&usg=AFQjCNG39FNkliecnIrk9jVZYEZEYQu37A&sig2=4lblLxdbBfCXi3WsKqsL8A');
			//ctaLayer.setMap(myMap);
			
			var main = "<span style='line-height:200%; top:20%; text-align:left;'>"
			for (dataSet in DataSources)
			{
				main +="<h2 class='button' onclick='switchSubject(" + dataSet + ")'>" + DataSources[dataSet].name + "</h2>";
			}
			main += "</span>";
			showNotification(main, 300, false, "<b>Welcome to the ToponymVis project!</b>"
				+ "<br><br>Choose your region of interest:"); 
		}
		
		/**
		* This function shows a notification window with all the possible sets. 
		* Information about avaliable sets should be kept in the DataSources array.
		*/
		function chooseSubject()
		{
			var main = "<span style='line-height:200%; top:20%; text-align:left;'>"
			for (dataSet in DataSources)
			{
				main +="<h2 class='button' onclick='switchSubject(" + dataSet + ")'>" + DataSources[dataSet].name + "</h2>";
			}
			main += "</span>";
			notification.style.top = "30%";
			showNotification(main, 240, true, "Choose your data:"); 
		}

		/**
		* Switches to the selected data set.
		*/		
		function switchSubject(target)
		{
			showNotification('Loading...', 100, false, false); 
			for (index in toponymsArray)
			{
				toponymsArray[index].hideCircle();
			}
			for (index in groups)
			{
				groups[index].hidePolygon();
			}
			
			groups = new Array();
			toponymsArray = new Array();
			groupsIndexesArray = new Array();
			currentSelection = new Array();
			
			$("#groups_place").html('');
			infowindow.close();
			
			textFileToToponyms(loadInfoFromFile(DataSources[target].fileName),DataSources[target].fileFormat);
			myMap.setCenter(DataSources[target].startPoint);
			$("#subject").html(DataSources[target].name);
			currentSource = target;
			
			getGroups();
			printOutGroups();
			resetMapView();
			deselectArea();
			
			if (DataSources[target].fileFormat.population){ $("#controlButton05").css("display","inline"); constructPopulationData();}
			else {$("#controlButton05").css("display","none");}
			
			if (useOverview){useOverview = false;} 
			else {useOverview = true;}
			overview();
			
			if (populationDensityLayerVisability){populationDensityLayerVisability = false;} 
			else {populationDensityLayerVisability = true;}
			togglePopulationDensity();
			
			hideNotification();	
		}
		
		/**
		* Shows a notification window with the information about the project.
		*/		
		function showAbout()
		{
			var about = loadInfoFromFile("about.txt");
			notification.style.top = "10%";
			notification.style.textAlign = "left";
			showNotification(about, 700, true, "<h4>About the project</h4>"); 
		}
		
		/**
		* Shows a notification window with the information about the authors.
		*/		
		function showAuthors()
		{
			var authors = "<span style='font-family:Tahoma; font-size:12px;'><table style='display:inline; text-align:center; margin:5px 10px;'><tr><td>Agnia Barsukova</td></tr> <tr><td><a href='mailto:agniabarsukova@gmail.com'>agniabarsukova@gmail.com</a></td></tr></table> "
						+ "<table style='display:inline; text-align:center; margin:5px 10px;'><tr><td>Daniil Sorokin</tr></td> <tr><td><a href='mailto:magpie.danial@gmail.com'>magpie.danial@gmail.com</a></td></tr></table><br>" 
						+ "<table style='display:inline; text-align:center; margin:5px 10px;'><tr><td>Vladlena Sergeeva</td></tr> <tr><td><a href='mailto:lada.sergeeva@gmail.com'>vlada.sergeeva@gmail.com</a></td></tr></table> "
						+ "<table style='display:inline; text-align:center; margin:5px 10px;'><tr><td>Ildus Mukhametov</td></tr> <tr><td><a href='mailto:ildus.mukhametov@gmail.com'>ildus.mukhametov@gmail.com</a></td></tr></table><br>" 
						+ "Eberhard Karls Universitaet Tuebingen<br> 2012 </span>";
			notification.style.top = "30%";
			notification.style.textAlign = "center";
			showNotification(authors, 500, true, "<h4>Contact</h4>"); 
		}

		/**
		* Prints out all the buttons (one for each the group) in the special area under the map.
		*/				
		function printOutGroups()
		{
			var formantsPanel = document.getElementById("formants_controls");
			//var formantsPanel = $("#formants_controls");
			if (formantsPanel.hasChildNodes())
				{
					while (formantsPanel.childNodes.length >= 1)
					{
						formantsPanel.removeChild(formantsPanel.firstChild);       
					} 
				}
			
			for (index in groupsIndexesArray)
			{
				formantsPanel.appendChild(groups[groupsIndexesArray[index]].formantButton);
				formantsPanel.appendChild(document.createTextNode(" "));
			}
			
			$("#numberOfGroups").html(groupsIndexesArray.length + " groups, " + toponymsArray.length + " toponyms");
		}
		
		/**
		* Sets the map's postion and the zoom level to the default values.
		*/
		function resetMapView()
		{
			myMap.setZoom(DataSources[currentSource].startZoom);
			myMap.setMapTypeId(google.maps.MapTypeId.ROADMAP);
			myMap.setCenter(DataSources[currentSource].startPoint);
		}
		
		/**
		* This function unselects all the buttons which were previously selected.
		*/
		function unselectAll()
		{
			for (group in groupsIndexesArray)
			{
				if (groups[groupsIndexesArray[group]].isSelected)
				{
					groups[groupsIndexesArray[group]].clicked();
				}
			}
		}
		
		/**
		* Shows up to 1000 thousand toponyms on the map in one color as a background.
		*/
		function overview()
		{
			var controlButton4 = document.getElementById("controlButton04");
			if (useOverview)
			{
				controlButton04.style.fontWeight = "normal";
				for (group in groupsIndexesArray)
				{
					if (!groups[groupsIndexesArray[group]].isSelected) groups[groupsIndexesArray[group]].hideCircles();				
				}
				useOverview = false;
			}
			else
			{
				controlButton04.style.fontWeight = "bold";
				for (group in groupsIndexesArray)
				{
					if (!groups[groupsIndexesArray[group]].isSelected) groups[groupsIndexesArray[group]].deHighlightCircles();
				}
				useOverview = true;
			}
		}
		
		/**
		* Switches on/off googleMaps lables on the map.
		*/
		function googleMapLabels()
		{
			var controlButton2 = document.getElementById("controlButton02");
			if(styleArray[3].stylers[0].visibility == "on")
			{
				styleArray[3].stylers[0].visibility = "off";
				controlButton2.style.fontWeight = "normal";
			}
			else
			{
				styleArray[3].stylers[0].visibility = "on";
				controlButton2.style.fontWeight = "bold";
			}
			myMap.setOptions({styles: styleArray});
		}
		
		/**
		* Switches on/off population density layer.
		*/
		function togglePopulationDensity()
		{
			if (populationDensityLayerVisability){
				populationDensityLayerVisability = false;
				$("#controlButton05").css("font-weight","normal");
				$("#populationDensityTable").css("display","none");
				for (p in populationPolygonsArray){
					populationPolygonsArray[p].setMap(null);
				}
			}
			else{
				populationDensityLayerVisability = true;
				$("#controlButton05").css("font-weight","bold");
				$("#populationDensityTable").css("display","inline");
				for (p in populationPolygonsArray){
					populationPolygonsArray[p].setMap(myMap);
				}
			}
		}
		
		/**
		* Enables an area selection mode or starts a new selection.
		*/
		function enableAreaSelection(){
			infowindow.close();
			$("#selectionToolIcon rect").attr("stroke-width","1.2");
			$("#selectionToolIcon rect").attr("stroke","#000");
			if(selectionPolyline) selectionPolyline.setMap(null);
			if(selectionMarker) selectionMarker.setMap(null);
			$("#dragMapIcon path").attr("stroke-width","0.8");
			$("#dragMapIcon path").attr("stroke","#444");
			myMap.setOptions({draggable:false});
			selectionEvent = google.maps.event.addListener(myMap, 'click', addPointToSelection);
			selectionPolyline = new google.maps.Polyline({
								strokeColor: SELECTION_COLOR,
								strokeOpacity: 1.0,
								strokeWeight: 2,
								map: myMap
								});
		}
		
		/**
		* Makes the map draggable.
		*/
		function enableDragAMap()
		{
			$("#selectionToolIcon rect").attr("stroke-width","0.8");
			$("#selectionToolIcon rect").attr("stroke","#444");
			$("#dragMapIcon path").attr("stroke-width","1.2");
			$("#dragMapIcon path").attr("stroke","#000");
			
			google.maps.event.removeListener(selectionEvent);
			if(selectionPolyline) selectionPolyline.setMap(null);
			if(selectionMarker) selectionMarker.setMap(null);
			myMap.setOptions({draggable:true});			
		}
		
		/**
		* Adds a new point to the current selection.
		*
		* @param event Google event
		*/
		function addPointToSelection(event){
			var path = selectionPolyline.getPath();
			if (path.getLength() == 0){
				var image = "<svg xmlns='http://www.w3.org/2000/svg' version='1.1' width='15px' height='15px' zoomAndPan='disable' style='cursor:pointer;'><circle cx='7.5' cy='7.5' r='7.5' opacity='0.55' stroke='#444' stroke-width='1' fill='" + SELECTION_COLOR + "'/></svg>"
				selectionMarker = new RichMarker({
									map: myMap,
									position: event.latLng,
									draggable: false,
									flat: true,
									anchor: RichMarkerPosition.MIDDLE,
									content: image,
									});
				google.maps.event.addListener(selectionMarker, 'click', completeSelectionPolygon);
			}
			path.push(event.latLng);
		}
		
		/**
		* Completes the selection and creates a polygon.
		*/
		function completeSelectionPolygon(){
			deselectArea();
			enableDragAMap();
			var path = selectionPolyline.getPath();
			selectionPolygon = new google.maps.Polygon({
							paths: path,
							clickable: false,
							strokeColor: SELECTION_COLOR,
							strokeOpacity: 0.4,
							strokeWeight: 2,
							fillColor: SELECTION_COLOR,
							map: myMap,
							fillOpacity: 0.05
							});
			
			google.maps.event.removeListener(selectionEvent);
			$("#controlButton08").css("display","inline");
			
			for (t in toponymsArray){
				var toponym = toponymsArray[t];
				if (google.maps.geometry.poly.containsLocation(new google.maps.LatLng(toponym.latitude, toponym.longitude), selectionPolygon)){
					toponym.active = true;
				}
				else {
					toponym.active = false;
				}
				toponym.updateVisualStatus();
			}
			
			var latlngbounds = new google.maps.LatLngBounds( );
			for (var i = 0; i < path.getLength(); i++ ){
				 latlngbounds.extend(path.getAt(i));
			}
			myMap.fitBounds(latlngbounds);
		}
		
		function deselectArea()
		{
			if(selectionPolygon) selectionPolygon.setMap(null);
			$("#controlButton08").css("display","none");
			for (t in toponymsArray){	
				 toponymsArray[t].active = true;
				 toponymsArray[t].updateVisualStatus();
			}
			if (useOverview){useOverview = false;} 
			else {useOverview = true;}
			overview();
		}
		
		/**
		* Show/hide polygons.
		*/
		function usePolygons()
		{
			var controlButton3 = document.getElementById("controlButton03");
			if (showPolygons)
			{
				showPolygons = false;
				controlButton03.style.fontWeight = "normal";
				for(group in groups)
				{
					groups[group].hidePolygon();
				}
			}
			else
			{
				showPolygons = true;
				controlButton03.style.fontWeight = "bold";
				for(group in groups)
				{
					if (groups[group].isSelected) groups[group].showPolygon();
				}
			}
		}
		
		/**
		* This function analyzes the current array of toponyms 
		* and builds initial groups by collecting toponyms with the same suffix into the same group.
		*/
		function getGroups()
		{
			var topoformants = new Array();
			for (toponymGG1 in toponymsArray)
			{
				var answer = topoformants.indexOf(toponymsArray[toponymGG1].formant);
				if (answer == -1)
				{
					topoformants.push(toponymsArray[toponymGG1].formant);
					groups.push(new Group (toponymsArray[toponymGG1].formant,[toponymGG1]));
					toponymsArray[toponymGG1].memberOf = groups.length-1;
				}
				else
				{
					groups[answer].toponyms.push(toponymGG1);
					toponymsArray[toponymGG1].memberOf = answer;
				}
				toponymsArray[toponymGG1].createInfoText();
				toponymsArray[toponymGG1].createCircle(null);
			}
			for (index in groups)
			{
				groups[index].toponyms.sort(function(a,b){
									var x = toponymsArray[a].name, y = toponymsArray[b].name;
									return x < y ? -1 : x > y ? 1 : 0;
								});
				groups[index].createPolygons();
				groups[index].createContentElement();
				groups[index].createFormantButton();
			}
			sortGroups(currentSortType);
		}
		
		function testPolygonIntersection(polygon1, polygon2)
		{
			if(polygon1 && polygon2)
			{
				var vertices = polygon1.getPath();
				if(google.maps.geometry.spherical.computeDistanceBetween(vertices.getAt(0),polygon2.getPath().getAt(0)) > 15000) return false;
				for (var i = 0; i < vertices.length; i++)
				{
					if (google.maps.geometry.poly.containsLocation(vertices.getAt(i), polygon2)) return true;
				}
			}
			return false;
		}
		
		/**
		* Computes a number of point to build a certain normal polygon.
		*
		* @param n number of vertices
		* @param r radius
		* @param lat center's latitude
		* @param lng center's longitude
		* @return path array of LatLng coordinates
		*/
		function constructNNormalPolygon(n, r, lat, lng)
		{
			var path = new Array();
			
			for (var i = 0; i < n; i++) 
			{
				path.push(new google.maps.LatLng(lat + r * Math.cos(2 * Math.PI * i / n), lng + (r * Math.sin(2 * Math.PI * i / n))/Math.cos(lat.toRad())));
			}
			return path;
		}
		
		/**
		* Combines two google polygons into one and returns it.
		*
		* @param polygon1 first polygon
		* @param polygon2 second polygon
		* @return google polygon
		*/
		function combinePolygons(polygon1, polygon2)
		{
			var newPath = polygon1.getPath();
			var vertices2 = polygon2.getPath();
			var newPath =[polygon1.getPath().getArray(),polygon2.getPath().getArray()];
			return new google.maps.Polygon({
								paths: newPath,
								clickable: false,
								strokeWeight: 0,
								fillColor: '#888',
								map: null,
								fillOpacity: 0.8});
		}
		
		/**
		* Deletes a particular value from an array.
		* 
		* @param Arr an array to process
		* @param deletValue a value need to be deletd
		* @return new array
		*/
		function deleteValues(Arr, deleteValue) 
		{
			for (var el = 0; el < Arr.length; el++) 
			{
				if (Arr[el] == deleteValue) 
				{         
					Arr.splice(el, 1);
					el--;
				}
			}
			return Arr;
		}
		
		/**
		* Construct a population density layer from the information about toponyms.
		*/
		function constructPopulationData()
		{
			for (p in populationPolygonsArray){
				populationPolygonsArray[p].setMap(null);
			}
			populationPolygonsArray = new Array();

			var populationPoints = new Array();
			for (t in toponymsArray){
				toponymInQuestion = toponymsArray[t];
				if (toponymInQuestion.hasGeoCoords){
					var hasHome = false;
					var nearestPointlat = Math.round(toponymInQuestion.latitude*10)/10;
					var nearestPointlng = Math.round(toponymInQuestion.longitude*10/2)/10*2;
					
					for (a in populationPoints){
						var populationPoint = populationPoints[a];
						if (Math.abs(populationPoint.lat - nearestPointlat) < 0.09 && Math.abs(populationPoint.lng - nearestPointlng) < 0.09){
							populationPoint.density = (populationPoint.density + toponymInQuestion.population/7)/2;
							hasHome = true;
						}
					}
					if (!hasHome){
						populationPoints.push({lat:nearestPointlat, lng:nearestPointlng, density:toponymInQuestion.population/7});
					}
				}
			}

			var populationClusters = [{from: 0, to: 10},{from: 10, to: 16},{from: 16, to: 30},{from: 30, to: 85},{from: 85, to: 160},{from: 160, to: 550},{from: 550, to: 1100},{from: 1100, to: 2500},{from: 2500, to: 5000},{from: 5500} ]
			for (c in populationClusters){
				populationClusters[c].points = new Array();
			}
			
			for (a in populationPoints){
				var populationPoint = populationPoints[a];
				var i = 9;
				while (i >= 0){
					if (populationPoint.density < populationClusters[i].from){i--;}
					else {populationClusters[i].points.push(populationPoint); i = -1;}
				}
			}
			
			var toColor = new Hex(0x660000);
			var fromColor = new Hex(0xFFFF99);
			var colors = fromColor.range(toColor, 10, true);

			for (c in populationClusters){
				var tmpPath = new Array();
				for (a in populationClusters[c].points){
					var populationPoint = populationClusters[c].points[a];
					tmpPath.push(constructNNormalPolygon(4, 0.05 + 0.1*Math.cos(populationPoint.lat.toRad()), populationPoint.lat, populationPoint.lng));
				}
				populationPolygonsArray.push(new google.maps.Polygon({
								paths: tmpPath,
								clickable: false,
								strokeWeight: 0,
								fillColor: '#'+colors[c].toHex(),
								map: null,
								fillOpacity: 0.5,
								zIndex: c-10}));
			}
			
			$("#populationDensityTable").html("");
			for(c in populationClusters)
			{
				$("#populationDensityTable").append("<tr><td style='background-color:" + '#'+colors[c].toHex() + ";width:25'></td><td style='padding:0px 5px'>" + ((populationClusters[c].to) ? populationClusters[c].from + " - " + populationClusters[c].to : "Over " + populationClusters[c].from) + "</td></tr>");
			}
		}
		
		/**
		* Sorts the groups according to the type parameter. 
		* This function affects only the print out order of the groups not the order in the groups array.
		*
		* @param type sort type. Can be "letters_back", "letter_front" or "size".
		*/
		function sortGroups(type)
		{
			switch(currentSortType)
			{
				default:
				case "letters_back":
					$("#optButton02").css("font-weight","normal");
					break;
				case "letters_front":
					$("#optButton04").css("font-weight","normal");
					break;
				case "size":
					$("#optButton03").css("font-weight","normal");
					break;
			}
			
			switch(type)
			{
				default:
				case "letters_back":
					$("#optButton02").css("font-weight","bold");
					groupsIndexesArray.sort(function(a,b){
									var x = groups[a].name.split("").reverse().join(""), y = groups[b].name.split("").reverse().join("");  
									return x < y ? -1 : x > y ? 1 : 0;
								} );
								break;
                case "letters_front":
					$("#optButton04").css("font-weight","bold");
					groupsIndexesArray.sort(function(a,b){
									var x = groups[a].name, y = groups[b].name;  
									return x < y ? -1 : x > y ? 1 : 0;
								} );
								break;
				case "size":
					$("#optButton03").css("font-weight","bold");
					groupsIndexesArray.sort(function(a,b){
									var x = groups[a].toponyms.length, y = groups[b].toponyms.length;  
									return y - x;
								} );
								break;		
			}
			currentSortType = type;
			printOutGroups();
		}
		
		/**
		* Finds the longest common string of two strings given as a parameters.
		* Base on the code from: http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Longest_common_substring
		*
		* @param first string
		* @param second string
		*/
		function longestCommonSubstring(string1, string2)
		{
			if (string1 == string2) return string1;
			
			// init max value
			var longestCommonSubstring = 0;
			var lastSubsBegin = 0;
			sequence = "";
			// init 2D array with 0
			var table = Array(string1.length);
			for(a = 0; a <= string1.length; a++){
					table[a] = Array(string2.length);
					for(b = 0; b <= string2.length; b++){
							table[a][b] = 0;
					}
			}
			// fill table
			for(var i = 0; i < string1.length; i++){
					for(var j = 0; j < string2.length; j++){
							if(string1[i]==string2[j]){
									if(table[i][j] == 0){
											table[i+1][j+1] = 1;
									} else {
											table[i+1][j+1] = table[i][j] + 1;
									}
									if(table[i+1][j+1] > longestCommonSubstring)
									{
										var thisSubsBegin = i + 1 - table[i+1][j+1];
										longestCommonSubstring = table[i+1][j+1];
										if (lastSubsBegin == thisSubsBegin)
										{//if the current LCS is the same as the last time this block ran
											sequence += string1.charAt(i);
										}
										else //this block resets the string builder if a different LCS is found
										{
											lastSubsBegin = thisSubsBegin;
											sequence = string1.substring(lastSubsBegin, i+1);
										}
									}
							} else {
								table[i+1][j+1] = 0;
							}
					}
			}
			return sequence;
		}
		
		function longestCommonSuffix(string1, string2, suff, pos) 
		{
			if (string1.length-1 == pos || string2.length-1 == pos
				|| (string1.charAt(string1.length-1 - pos) != string2.charAt(string2.length-1 - pos)))
			{
				return suff;
			}
			suff = string1.charAt(string1.length - 1 - pos) + suff;
			pos++;
			return longestCommonSuffix(string1, string2, suff, pos);
		}

		/**
		* Merges all of the seleced groups into one.
		*/
		function mergeGroups()
		{
			var tmpFormant = groups[currentSelection[0]].formant;
			var tmpToponyms = new Array();
			var tmpContains = new Array();
			for (groupId in currentSelection)
			{
				var group = groups[currentSelection[groupId]];
				tmpFormant = longestCommonSubstring(group.formant, tmpFormant);
				//tmpFormant = longestCommonSuffix(group.formant, tmpFormant, "", 0);
				tmpToponyms = tmpToponyms.concat(group.toponyms);
				group.isSelected = false;
				group.hideCircles();
				group.hidePolygon();
				group.formantButton.style.backgroundColor = "";
				document.getElementById("groups_place").removeChild(group.groupContentElement);
				groupsIndexesArray.splice(groupsIndexesArray.indexOf(currentSelection[groupId]),1);
				tmpContains.push(currentSelection[groupId]);
			}
			currentSelection = new Array();
			
			groups.push(new Group (tmpFormant,tmpToponyms));
			groups[groups.length-1].contains = tmpContains;
			groups[groups.length-1].name = "-" + groups[groups.length-1].formant + "-";
			groups[groups.length-1].toponyms.sort(function(a,b){
									var x = toponymsArray[a].name, y = toponymsArray[b].name;
									return x < y ? -1 : x > y ? 1 : 0;
								});
			
			groups[groups.length-1].createPolygons();
			groups[groups.length-1].createContentElement();
			groups[groups.length-1].createFormantButton();
			
			for (toponymMG in tmpToponyms)
			{
				toponymsArray[tmpToponyms[toponymMG]].memberOf = groups.length-1;
			}

			sortGroups(currentSortType);
			groups[groups.length-1].clicked();
		}
		
		/**
		* Performs amoung selected groups the unmerge function for all complex groups (i.e. previously constructed by means of mergeGroups function).
		*/
		function unmergeGroups()
		{
			var toClick = new Array();
			for (groupId in currentSelection)
			{
				var group = groups[currentSelection[groupId]];
				if(group.contains && group.contains.length > 0)
				{
					groupsIndexesArray.splice(groupsIndexesArray.indexOf(currentSelection[groupId]),1);
					//group.clicked();
					toClick.push(currentSelection[groupId]);
					for (containedGroup in group.contains)
					{
						groupsIndexesArray.push(group.contains[containedGroup]);
						//groups[group.contains[containedGroup]].clicked();
						toClick.push(group.contains[containedGroup]);
						for (toponymUG in groups[group.contains[containedGroup]].toponyms)
						{
							toponymUGid = groups[group.contains[containedGroup]].toponyms[toponymUG];
							toponymsArray[toponymUGid].memberOf = group.contains[containedGroup];
						}
					}
				}
			}
			for (groupToClick in toClick)
			{
				groups[toClick[groupToClick]].clicked();
			}
			sortGroups(currentSortType);
		}
		
		/**
		 * Use JQuery to get text data from file
		 * @return loaded text
		 */
		function loadInfoFromFile(fileUrl)
		{
			return $.ajax({url: fileUrl, async:false}).responseText;
		}
		
		/**
		 * Function to parse tab delimitted text data: each line = one toponym.
		 * @return an array of objects
		 */
		function textFileToToponyms(data, format)
		{
			var lines = data.split("\n");
			for (line in lines)
			{
				var features = lines[line].replace(/[\n\r]/gi,"").split("\t");
				if (Number(features[format.latitude]) != 0.0 &&
					Number(features[format.longitude]) != 0.0)
				{
					toponymsArray.push(new Toponym(features[format.name], Number(features[format.latitude]), Number(features[format.longitude]), features[format.formant], true));
				}
				else
				{
					toponymsArray.push(new Toponym(features[format.name], Number(features[format.latitude]), Number(features[format.longitude]), features[format.formant], false));
				}
				if(format.population) toponymsArray[toponymsArray.length-1].population = Number(features[format.population]);
				if(format.otherNames) toponymsArray[toponymsArray.length-1].otherNames = features[format.otherNames];
			}
		}
		
		/**
		* Shows the notification window.
		*
		* @param message main html code to place into the notification window.
		* @param notificationWidth width of the notification window.
		* @param showCloseButton true if close button should be visiable; false for opposite.
		* @param header the header of the notification window.
		*/
		function showNotification(message, notificationWidth, showCloseButton, header)
		{
			var notificationHeader = document.getElementById("notificationHeader");
			var notificationCloseButton = document.getElementById("notificationCloseButton");
			var notificationMessage = document.getElementById("notificationMessage");

			if (header)
			{
				notificationHeader.innerHTML = header;
			}
			else
			{
				notificationHeader.innerHTML = "";
			}
			if (showCloseButton)
			{
				notificationCloseButton.style.display = "inline"; 
			}
			else
			{
				notificationCloseButton.style.display = "none"; 
			}
			
			notificationMessage.innerHTML = message;
			
			notification.style.width = notificationWidth;
			notification.style.left = (document.body.clientWidth - notificationWidth)/2;
			
			notification.style.display = 'inline';
			//screen.style.display = 'inline';
		}
		
		/**
		* Hides notifictaion window.
		*/
		function hideNotification()
		{
        	notification.style.display = 'none';
			//screen.style.display = 'none';
		}
