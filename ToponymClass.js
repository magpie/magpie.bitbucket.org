/*!
 * Copyright (C) 2012  Agnia Barsukova, Daniil Sorokin, Ildus Mukhametov, Vladlena Sergeeva, 
 * Department of General and Computational Linguistics, University of Tuebingen
 * 
 * This file is part of the ToponymVis project.
 *
 * ToponymVis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.*
 *
 * ToponymVis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details: <http://www.gnu.org/licenses/>.
 *
 * Contact: magpie.danial@gmail.com, agniabarsukova@gmail.com
 */


/*
 * This file contains the toponym class for the project.
 * Toponym class contains all the infromation about the toponym and its representation on the map.
*/
		/**
		* Constructor.
		* 
		* @constructor
		* @param name
		* @param latitude
		* @param longitude
		* @param formant toponymic suffix
		* @param hasGeoCoords true if there are geo coordinates available for this toponym.
		*/
		function Toponym(name, latitude, longitude, formant, hasGeoCoords)
		{
			this.name = name; // Name of the toponym.
			this.latitude = latitude;
			this.longitude = longitude;
			this.formant = formant.replace(/[\(\)\']/gi, "");
			this.hasGeoCoords = hasGeoCoords;
			this.circle;
			this.id = toponymsArray.length;
			//this.population = Math.random()*100000;
			this.memberOf;
			this.active = true;
		}
		
		/**
		* Creates a text to be put into infowindow for this toponym.
		*/
		Toponym.prototype.createInfoText =  function ()
		{
			this.infoText = "<h3>" + this.name + "</h3>" +
								"<span style='color:#707070'>Latitude:</span> " + this.latitude + "&nbsp&nbsp&nbsp<span style='color:#707070'>Longitude:</span> " +this.longitude + "<br>" +
								"<span style='color:#707070'>Formant:</span> " + this.formant + "<br>";
			if (this.population) {this.infoText += "<span style='color:#707070'>Population in this area:</span> " + this.population + "<br>";}
			if (this.otherNames) this.infoText += "<span style='color:#707070'>Other names:</span> " + this.otherNames;
		}
		
		/**
		* Creates a circle to represent the toponym on the map.
		*/
		Toponym.prototype.createCircle =  function (map)
		{
			var opacity = DEFAULT_OPACITY;
			var radius = 750;
			if (this.population)
			{
				radius = this.population / 100;
				if (this.population > 80000) opacity = 1 / (this.population / 50000);
				
				if (radius < 750) radius = 750;
				if (opacity < 0.4) opacity = 0.4;
			}
			
			var circleOptions = {
				  strokeColor: DEFAULT_COLOR,
				  strokeOpacity: opacity,
				  strokeWeight: 1,
				  fillColor: DEFAULT_COLOR,
				  fillOpacity: opacity - 0.3,
				  map: map,
				  center: new google.maps.LatLng(this.latitude, this.longitude),
				  radius: radius
				};
			this.circle = new google.maps.Circle(circleOptions);
			var currentCenter = this.circle.getCenter();
			var currentId = this.id;
			var currentContent = this.infoText;
			google.maps.event.addListener(this.circle, 'click', function() {if (!groups[toponymsArray[currentId].memberOf].isSelected) groups[toponymsArray[currentId].memberOf].clicked(); infowindow.setContent(currentContent); infowindow.setPosition(currentCenter); infowindow.open(myMap);});
		}
		
		/**
		* Function to change the circle's color.
		*
		* @param newColor new color of the circle.
		*/
		Toponym.prototype.modifyCircle = function (newColor)
		{
			this.circle.setOptions({strokeColor: newColor, fillColor: newColor});
		}
		
		/**
		* Hides circle.
		*/
		Toponym.prototype.hideCircle = function()
		{
			if (!(this.circle.getMap() == null)) this.circle.setMap(null);
		}
		
		/**
		* Places circle on the map.
		*/
		Toponym.prototype.showCircle = function()
		{
			if (this.active && this.circle.getMap() == null) this.circle.setMap(myMap);
		}
		
		/**
		* Brings the visual characteristics of the toponym into correspondence with its current parameters.
		*/
		Toponym.prototype.updateVisualStatus = function()
		{
			if (this.active){
				if(groups[this.memberOf].isSelected){
					this.modifyCircle(groups[this.memberOf].color);
					this.showCircle();
				}
				/*else if(useOverview){
					this.modifyCircle(DEFAULT_COLOR);
					this.showCircle();
				}*/
			}
			else{
				this.hideCircle();
			}
		}