/*!
 * Copyright (C) 2012  Agnia Barsukova, Daniil Sorokin, Ildus Mukhametov, Vladlena Sergeeva, 
 * Department of General and Computational Linguistics, University of Tuebingen
 * 
 * This file is part of the ToponymVis project.
 *
 * ToponymVis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.*
 *
 * ToponymVis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details: <http://www.gnu.org/licenses/>.
 *
 * Contact: magpie.danial@gmail.com, agniabarsukova@gmail.com
 */


﻿/*
 * This file contains the group class for the project. 
 * This class represents a group of toponyms, all of its representations on the map 
 * and also GUI elments connected to the  group.
 */
		var latinLetterColorMap = {a: "#ff0000", e: "#ff4500", i: "#ff8c00", o: "#cd853f", u: "#a52a2a", 
		y: "#f4a460", ä: "#ff6347", ö: "#bc8f8f", ü: "#cd5c5c", b: "#000080", c: "#6a5acd", d: "#0000ff", 
		f: "#00bfff", g: "#00ffff", h: "#5f9ea0", j: "#006400", k: "#00ff7f", l: "#7fff00", m: "#32cd32", 
		n: "#6b8e23", p: "#556b2f", q: "#9370db", r: "#a020f0", s: "#ee82ee", t: "#c71585", v: "#db7093", 
		w: "#ff1493", x: "#ff69b4", z: "#daa520", ß: "#8fbc8f"};
		
		var russianLetterColorMap = {а: "#ff0000", е: "#ff4500", ё: "#bc8f8f", и: "#ff8c00", о: "#cd853f", 
		у: "#a52a2a", ы: "#f4a460", э: "#ff6347", ю: "#cd5c5c", я: "#ff4040", б: "#000080", в: "#db7093", 
		г: "#00ffff", д: "#0000ff", ж: "#ff69b4", з: "#daa520", й: "#006400", к: "#00ff7f", л: "#7fff00", 
		м: "#32cd32", н: "#6b8e23", п: "#556b2f", р: "#a020f0", с: "#ee82ee", т: "#c71585", ф: "#00bfff", 
		х: "#5f9ea0", ц: "#daa520", ч: "#6a5acd", ш: "#ff1493", щ: "#8fbc8f", ъ: "#545454", ь: "#9370db"};

		/*
		* Taken from: http://www.movable-type.co.uk/scripts/latlong.html
		*/
		if (typeof(Number.prototype.toRad) === "undefined") 
		{
			Number.prototype.toRad = function() {
			return this * Math.PI / 180;
			}
		}
		
		/**
		* Constructor.
		* 
		* @constructor
		* @parmam formst toponymic suffix of the group.
		* @param toponyms an array of toponyms to be members of the group.
		*/
		function Group(formant, toponyms)
		{
			this.formant = formant;
			if (this.formant.length > 0) {this.name = this.formant;} else {this.name = "Empty suffix";}
			this.toponyms = toponyms;
			this.isSelected = false;
			this.id = groups.length;
			groupsIndexesArray.push(this.id);
			
			this.contains; // array to contain other groups inside of this one.
			this.polygon;
			this.color;
			
			this.polygons = new Array();
			this.formantButton = document.createElement("span");			
			this.groupContentElement = document.createElement("span");
			
			this.getColor();
		}
		
		/**
		* Function to assign a color based on the formant to the group. Color is stored in the "color" parameter of the group.
		*/
		Group.prototype.getColor = function ()
		{
		    //if the formant is empty, i.e. we're in "all the rest" group, assign dark-gray color
			var sourceString = this.formant.replace(/[\*\-]/,"");
			if (sourceString.length == 0)
		    {
		        this.color = "#101010";
		    }
		    //if we're dealing with Russian
		    else if (russianLetterColorMap[sourceString.charAt(0)])
		    {
		        //if the format is 1 letter-long, just return the color value for that letter
		        if (sourceString.length == 1)
		        {
		            this.color = russianLetterColorMap[sourceString];
		        }
		        //otherwise add up rgb values, with each consequent letter (from the end of formant backwards) having less importance
		        else 
		        {
		            var redInt = 0;
		            var greenInt = 0;
		            var blueInt = 0;
		            var factor = 0.5;
		            for (var i = sourceString.length - 2; i >= 0; i--)
		            {
						var red = parseInt(russianLetterColorMap[sourceString.charAt(i)].substr(1, 2), 16);
						var green = parseInt(russianLetterColorMap[sourceString.charAt(i)].substr(3, 2), 16);
						var blue = parseInt(russianLetterColorMap[sourceString.charAt(i)].substr(5, 2), 16);
						redInt += red * factor;
						greenInt += green * factor;
						blueInt += blue * factor;
						factor = factor / 2;
		            }
		            var redHex = Math.round(redInt).toString(16);
		            if (redHex.length == 1)
		            {
		                redHex = "0" + redHex;
		            }
		            var greenHex = Math.round(greenInt).toString(16);
		            if (greenHex.length == 1)
		            {
		                greenHex = "0" + greenHex;
		            }
		            var blueHex = Math.round(blueInt).toString(16);
		            if (blueHex.length == 1)
		            {
		                blueHex = "0" + blueHex;
		            }
		            this.color = "#" + redHex + greenHex + blueHex;
		        }
		    }
		    //if we're dealing with German
		    else
		    {
		        //if the format is 1 letter-long, just return the color value for that letter
		        if (sourceString.length == 1)
		        {
		            this.color = latinLetterColorMap[sourceString];
		        }
		        //otherwise add up rgb values, with each consequent letter (from the end of formant backwards) having less importance
		        else 
		        {
		            var redInt = 0;
		            var greenInt = 0;
		            var blueInt = 0;
		            var factor = 0.5;
		            for (var i = sourceString.length - 2; i >= 0; i--)
		            {
						var red = parseInt(latinLetterColorMap[sourceString.charAt(i)].substr(1, 2), 16);
						var green = parseInt(latinLetterColorMap[sourceString.charAt(i)].substr(3, 2), 16);
						var blue = parseInt(latinLetterColorMap[sourceString.charAt(i)].substr(5, 2), 16);
						redInt += red * factor;
						greenInt += green * factor;
						blueInt += blue * factor;
						factor = factor / 2;
		            }
		            var redHex = Math.round(redInt).toString(16);
		            if (redHex.length == 1)
		            {
		                redHex = "0" + redHex;
		            }
		            var greenHex = Math.round(greenInt).toString(16);
		            if (greenHex.length == 1)
		            {
		                greenHex = "0" + greenHex;
		            }
		            var blueHex = Math.round(blueInt).toString(16);
		            if (blueHex.length == 1)
		            {
		                blueHex = "0" + blueHex;
		            }
		            this.color = "#" + redHex + greenHex + blueHex;
		        }
		    }
		}
		
		/**
		* Construct a list of names of toponyms contained in the group.
		*
		* @return names
		*/
		Group.prototype.getNames = function()
		{
			var names = new Array();
			for (toponym in this.toponyms)
			{
				var tmpNameGroup = toponymsArray[this.toponyms[toponym]].name;
				if (toponymsArray[this.toponyms[toponym]].hasGeoCoords)
				{
					var tmpNameGroup = "<span class = 'buttonSimple' onclick = 'infowindow.setContent(toponymsArray["+ this.toponyms[toponym] + "].infoText); infowindow.setPosition(toponymsArray["+ this.toponyms[toponym] + "].circle.getCenter()); infowindow.open(myMap);' >" + toponymsArray[this.toponyms[toponym]].name + "</span>";
					names.push(tmpNameGroup);									 
				}
				else
				{
					names.push(toponymsArray[this.toponyms[toponym]].name + " (<span style=\"color:red\">Has no location data!</span>)");
				}
			}
			return names;
		}
		
		/**
		* Highlight all the circles (one for each toponym in the group) on the map using the color of the group.
		*/
		Group.prototype.highlightCircles = function()
		{
			for (toponym in this.toponyms)
			{
				toponymsArray[this.toponyms[toponym]].modifyCircle(this.color);
				toponymsArray[this.toponyms[toponym]].showCircle();
			}
			
		}
		
		/**
		* Remove from the map all the toponyms contained in th group.
		*/
		Group.prototype.hideCircles = function()
		{
			for (toponym in this.toponyms)
			{
				toponymsArray[this.toponyms[toponym]].hideCircle();
			}		
		} 
		
		/**
		* Highlight all the circles (one for each toponym in the group) on the map using the default color.
		*/ 
		Group.prototype.deHighlightCircles = function()
		{
			var tmpThisToponyms = this.toponyms.slice(0, 800/groupsIndexesArray.length);
			for (toponym in tmpThisToponyms)
			{
				toponymsArray[tmpThisToponyms[toponym]].showCircle();
				toponymsArray[tmpThisToponyms[toponym]].modifyCircle(DEFAULT_COLOR);
			}
		}
		
		/**
		* Performs a click on the button which represents the group in the GUI.
		*/
		Group.prototype.clicked = function()
		{
			if (this.isSelected)
			{
				this.isSelected = false;
				currentSelection.splice(currentSelection.indexOf(this.id),1);
				if (useOverview)
				{	
					this.deHighlightCircles();
					var tmpThisToponyms = this.toponyms.slice(800/groupsIndexesArray.length);
					for (toponym in tmpThisToponyms)
					{
						toponymsArray[tmpThisToponyms[toponym]].hideCircle();
					}
				}
				else
				{
					this.hideCircles();
				}
				this.hidePolygon();
				this.formantButton.style.backgroundColor = "";
				document.getElementById("groups_place").removeChild(this.groupContentElement);
				if (this.contains && this.contains.length > 0) document.getElementById("optButton06").style.display = 'none';
			}
			else
			{
				this.isSelected = true;
				currentSelection.push(this.id);
				if (showPolygons) this.showPolygon();
				this.highlightCircles();
				this.formantButton.style.backgroundColor = this.color;
				document.getElementById("groups_place").appendChild(this.groupContentElement);
				if (this.contains && this.contains.length > 0) document.getElementById("optButton06").style.display = 'inline';
			}
			
			if (currentSelection.length == 2) {
			document.getElementById("optButton05").style.display = 'inline';
			} else if (currentSelection.length < 2) {
			document.getElementById("optButton05").style.display = 'none';}
		}
		
		/**
		* Creates an html element of the page which contains the information about the group. 
		* This function is usually called only once after the group's creation.
		*/
		Group.prototype.createContentElement = function()
		{
			this.groupContentElement.setAttribute("style","position:relative");
			this.groupContentElement.setAttribute("id","contentElGrId" + this.id);
			var text = "<table width='100%'><tr><td><h4><svg xmlns='http://www.w3.org/2000/svg' version='1.1' width='20px' height='12px' zoomAndPan='disable' style='display:inline'><circle cx='6' cy='6' r='5' stroke='#444' stroke-width='1' fill='" + this.color + "'/></svg><span style='position: relative;top: -2px;'>"
						+ "<span class='groupName'><span class='buttonSimple' onclick='groups[" + this.id + "].editNameInContentElement()'>'" + this.name + "'</span> group</span>. ";
			if (this.formant.length > 0) {text += "Common suffix: <span style='color: black;'>" + this.formant + ".</span> ";} else {text += "Common suffix: Empty suffix. ";}
			text += "<span style='font-size:12;'>" + this.toponyms.length;
			if (this.toponyms.length > 1) {text += " toponyms.</span>";} else {text += " toponym.</span>";}
			
			if (this.contains && this.contains.length > 0)
			{
				text += " <span style='font-size:12;'>Contains "+ this.contains.length + " groups: ";
				for (groupCCE in this.contains)
				{
					text += "'" + groups[this.contains[groupCCE]].formant + "' group (" + groups[this.contains[groupCCE]].toponyms.length + "), ";
				}
				text = text.substring(0,text.length-2) + "</span>";
			}
			
			text += "</span></h4></td>";

			text += "<td style='text-align:right'><svg xmlns='http://www.w3.org/2000/svg' version='1.1' width='10px' height='10px' zoomAndPan='disable' style='cursor:pointer;' class='closeSign' onclick='groups[" + this.id + "].clicked()'><line x1='0' y1='0' x2='10' y2='10'/><line x1='0' y1='10' x2='10' y2='0'/></svg></td></tr></table>";
			text += "<span style='font-size:14px'>";
			names = this.getNames();
			for (name in names)
			{
				text += names[name] + ", ";
			}
			text = text.substring(0,text.length-2) + "<br></span><hr>";
			this.groupContentElement.innerHTML = text;
		}
		
		/**
		* Shows a textbox instead of the name of the group to edit it 
		* inside of the element which contains the information about the group.
		*/
		Group.prototype.editNameInContentElement = function()
		{
			$("#contentElGrId" + this.id +" .groupName").html("<input type='text' name='newGroupName' value='" + this.name + "' style='width:100px; height:18px; font-size:14px;'/> <span class='buttonSimple' style='color:green;font-size:12px' onclick='groups[" + this.id + "].submitNameInContentElement()'>OK</span>");
		}
		
		/**
		* Submits the new name of the group.
		*/
		Group.prototype.submitNameInContentElement = function()
		{
			this.name = $("#contentElGrId" + this.id +" input:text[name='newGroupName']").val();
			$("#contentElGrId" + this.id + " .groupName").html("<span class='buttonSimple' onclick='groups[" + this.id + "].editNameInContentElement()'>'" + this.name + "'</span> group");
			$("span#" + this.id + " .name").html(this.name);
		}
		
		/**
		* Creates a set of polygons to represent the group. 
		* This function is usually called only once after the group's creation.
		*/
		Group.prototype.createPolygons = function()
		{
			var tmpSetsArray = new Array(); 
			for (toponymCPs1 in this.toponyms)
			{
				if (toponymsArray[this.toponyms[toponymCPs1]].hasGeoCoords)
				{
					var tmpToponym = toponymsArray[this.toponyms[toponymCPs1]];
					var wasAddedTo = -1;
					var wasAddedDistance = 50000;
					for (set in tmpSetsArray)
					{
						var distance = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(tmpToponym.latitude, tmpToponym.longitude), new google.maps.LatLng(tmpSetsArray[set].center[0], tmpSetsArray[set].center[1]));
						if(distance < 50000 && distance < wasAddedDistance)
						{
							//tmpSetsArray[set].center = [(tmpSetsArray[set].center[0] + tmpToponym.latitude)/2, (tmpSetsArray[set].center[1] + tmpToponym.longitude)/2];
							wasAddedTo = set;
							wasAddedDistance = distance;
						}
						
					}
					
					if(wasAddedTo == -1)
					{
						tmpSetsArray.push({center:[tmpToponym.latitude, tmpToponym.longitude], toponyms:[this.toponyms[toponymCPs1]]});
					}
					else
					{
						tmpSetsArray[wasAddedTo].toponyms.push(this.toponyms[toponymCPs1]);
					}
				}
			}
			for (set in tmpSetsArray)
			{
				this.createPolygon(tmpSetsArray[set].toponyms);
			}
		}
		
		/**
		* Creates a polygon.
		* This function is called only from createPolygons()function.
		*/
		Group.prototype.createPolygon = function (toponymsSet)
		{
			var tmpArray = new Array();
			for (toponymCP1 in toponymsSet)
			{
				if (toponymsArray[toponymsSet[toponymCP1]].hasGeoCoords)
				{
					tmpArray.push([toponymsArray[toponymsSet[toponymCP1]].latitude + 0.025, toponymsArray[toponymsSet[toponymCP1]].longitude + 0.05]);
					tmpArray.push([toponymsArray[toponymsSet[toponymCP1]].latitude + 0.025, toponymsArray[toponymsSet[toponymCP1]].longitude - 0.05]);
					tmpArray.push([toponymsArray[toponymsSet[toponymCP1]].latitude - 0.025, toponymsArray[toponymsSet[toponymCP1]].longitude + 0.05]);
					tmpArray.push([toponymsArray[toponymsSet[toponymCP1]].latitude - 0.025, toponymsArray[toponymsSet[toponymCP1]].longitude - 0.05]);
				}
			}
			tmpArray = d3.geom.hull(tmpArray);
			var coordinatesArray = new Array();
			for (coordinates in tmpArray)
			{
				coordinatesArray.push(new google.maps.LatLng(tmpArray[coordinates][0], tmpArray[coordinates][1]));
			}
			
			var polygonOptions = {
							paths: coordinatesArray,
							clickable: false,
							strokeColor: this.color,
							strokeOpacity: 0.5,
							strokeWeight: 2,
							fillColor: this.color,
							map: null,
							fillOpacity: 0.2};
			//this.polygon = new google.maps.Polygon(polygonOptions);
			this.polygons.push(new google.maps.Polygon(polygonOptions));
		}

		
		/**
		* Removes all polygons of this group from the map.
		*/
		Group.prototype.hidePolygon = function()
		{
			for(polygon in this.polygons)
			{
				if (!(this.polygons[polygon].getMap() == null)) this.polygons[polygon].setMap(null);
			}
		}

		/**
		* Places all polygons of this group on the map.
		*/		
		Group.prototype.showPolygon = function()
		{
			for(polygon in this.polygons)
			{
				if (this.polygons[polygon].getMap() == null) this.polygons[polygon].setMap(myMap);
			}
		}

		/**
		* Creates an html button to provide an ability to select this group in the GUI. 
		* This function is usually called only once after the group's creation.
		*/
		Group.prototype.createFormantButton = function()
		{
			this.formantButton.innerHTML='';
			this.formantButton.setAttribute("id", this.id);
			this.formantButton.setAttribute("class", "button");
			this.formantButton.setAttribute("style", "border-style:solid;");
			this.formantButton.setAttribute("onclick", "groups[" + this.id + "].clicked()");
			if(this.isSelected) this.formantButton.style.backgroundColor = this.color;
			
			var fButtonName = document.createElement("span");
			fButtonName.setAttribute("class", "name");
			var fButtonNameText = document.createTextNode(this.name);
			fButtonName.appendChild(fButtonNameText);
            
			
            this.formantButton.appendChild(fButtonName);
			
			var fButtonNumber = document.createElement("span");
			fButtonNumber.setAttribute("style", "color:#888888;");
			var fButtonNumberText = document.createTextNode("\u00A0(" + this.toponyms.length + ")");
			fButtonNumber.appendChild(fButtonNumberText);
            this.formantButton.appendChild(fButtonNumber);

			if (this.contains && this.contains.length > 0)
			{
				var fButtonComplex = document.createElement("span");
				fButtonComplex.setAttribute("name","expandGroup");
				fButtonComplex.innerHTML = " <svg xmlns='http://www.w3.org/2000/svg' version='1.1' width='10px' height='12px' zoomAndPan='disable' style='display:inline'><polygon points='2,0 2,12 8,6' style='fill:#888;'/></svg>";
				this.formantButton.appendChild(fButtonComplex);
				/*
				for (groupCFB in this.contains)
				{
					groups[this.contains[groupCFB]].formantButton.style.display = "none";
					this.formantButton.appendChild(groups[this.contains[groupCFB]].formantButton);
				}*/
			}		
		}
